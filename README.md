# Atrify Challenge

## How to start the API

The API consists of a single Spring Boot Application. Simply clone and build the repo, then start the app within the file "DonutQueueApplication" - and then you can call the endpoints from your localhost. ie http://localhost:8081/order. 

(You can set the port to be whatever you want using a server.port property in a local application.properties file). Ie server.port=8081

## API Documentation
 Find the API Docs here - https://documenter.getpostman.com/view/25856126/2s93CNMt1y

## Future TODOs for the API

With more time, I would 
1. Write more unit and integration tests to increase coverage and ensure functionality. 
2. Break up the logic in OrderQueue.java so that the class isn't so big and isn't doing so much. 
3. Make a functional change. Make it so that cancelling a order has a side effect on the approximate wait time for the later orders. 
4. Consider error handling more. 
