package atrify.domain;

import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Queue;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import atrify.model.Delivery;
import atrify.model.Order;
import atrify.model.OrderPositionDetails;
import atrify.utils.Constants;

@Component
@Scope("singleton")
public class OrderQueue {
    private Queue<Order> orderStorage = new PriorityQueue<Order>(new CustomComparator());
    private ArrayList<Order> cancelledOrders = new ArrayList<Order>();

    public void addOrderToQueue(Order orderToAdd) {
        int orderQueuePosition = orderStorage.size() + 1;
        int approximateWaitTimeInMinutes = getApproximateWaitTimeInMinutes(orderToAdd.getDonutQuantity());

        orderToAdd.setOrderPositionDetails(
                new OrderPositionDetails(orderQueuePosition, approximateWaitTimeInMinutes));

        orderStorage.add(orderToAdd);
    }

    public Order getHeadFromQueue() {
        return orderStorage.poll();
    }

    public List<Order> getAllOrders() {
        final Iterator<Order> iterate = orderStorage.iterator();
        final List<Order> allOrders = new ArrayList<Order>();

        while (iterate.hasNext()) {
            allOrders.add(iterate.next());
        }

        return allOrders;
    }

    public Order getOrder(int idOfOrderToGet) {
        final Iterator<Order> iterate = orderStorage.iterator();

        while (iterate.hasNext()) {
            final Order orderToCheck = iterate.next();
            if (orderToCheck.getClientId() == idOfOrderToGet) {
                return orderToCheck;
            }
        }
        return null;
    }

    public OrderPositionDetails getOrderPositionDetails(int idOfOrder) {
        final Order order = getOrder(idOfOrder);
        return order.getOrderPositionDetails();
    }

    public List<OrderPositionDetails> getAllOrderPositionDetails() {
        final List<Order> allOrders = getAllOrders();
        return allOrders.stream()
                .map((order) -> order.getOrderPositionDetails()).toList();
    }

    // Can potentially move this out to the order service
    public Delivery createDelivery() {
        List<Order> ordersOfDelivery = new ArrayList<Order>();
        int numberOfDonutsInDelivery = 0;

        final Iterator<Order> iterate = orderStorage.iterator();

        while (iterate.hasNext()
                && (orderStorage.peek().getDonutQuantity() + numberOfDonutsInDelivery <= Constants.DONUT_BATCH_SIZE)) {
            final Order orderToCheck = iterate.next();
            ordersOfDelivery.add(orderToCheck);
            numberOfDonutsInDelivery += orderToCheck.getDonutQuantity();
        }

        ordersOfDelivery.stream().forEach((order) -> orderStorage.remove(order));

        return new Delivery(ordersOfDelivery);
    }

    public void refreshOrderPositionDetails(int cancelledOrderQueuePosition) {
        final Iterator<Order> iterate = orderStorage.iterator();
        int counter = 1;
        int queuePositionCount = cancelledOrderQueuePosition;
        Order orderToCancel = null;

        while (iterate.hasNext()) {
            final Order orderToCheck = iterate.next();
            if (counter == cancelledOrderQueuePosition) {
                cancelledOrders.add(orderToCheck);
                orderToCancel = orderToCheck;
            } else if (counter > cancelledOrderQueuePosition) {
                orderToCheck.getOrderPositionDetails().setQueuePosition(queuePositionCount);
                queuePositionCount++;
            }
            counter++;
        }
        orderStorage.remove(orderToCancel);
    }

    private int getApproximateWaitTimeInMinutes(int newOrderDonutQuantity) {
        final Iterator<Order> iterate = orderStorage.iterator();
        int numberOfDonutsToBeProcessedBeforeGivenOrder = 0;

        while (iterate.hasNext()) {
            final Order orderToCheck = iterate.next();
            numberOfDonutsToBeProcessedBeforeGivenOrder += orderToCheck.getDonutQuantity();
        }
        return calculateApproximateWaitTimeInMinutes(
                numberOfDonutsToBeProcessedBeforeGivenOrder + newOrderDonutQuantity);
    }

    private int calculateApproximateWaitTimeInMinutes(int numberOfDonutsToBeProcessedForGivenOrder) {
        double numberOfDonutBatches = Math
                .ceil((double) numberOfDonutsToBeProcessedForGivenOrder / Constants.DONUT_BATCH_SIZE);

        int totalTimeInMinutesToFinishBatches = (int) numberOfDonutBatches * Constants.MINUTES_PER_BATCH;

        return totalTimeInMinutesToFinishBatches;
    }

    private class CustomComparator implements Comparator<Order> {

        @Override
        public int compare(Order order1, Order order2) {
            final boolean order1Premium = order1.isPremium();
            final boolean order2Premium = order2.isPremium();

            if (!order1Premium && order2Premium) {
                return 1;
            } else if (order1Premium && order2Premium
                    || !order1Premium && !order2Premium) {
                return 0;
            } else {
                return -1;
            }
        }
    }
}
