package atrify.utils;

public class Constants {
    public static final int MINUTES_PER_BATCH = 5;
    public static final int DONUT_BATCH_SIZE = 50;
    public static final int MINIMUM_VALUE_OF_CLIENT_ID = 1;
    public static final int MAXIMUM_VALUE_OF_CLIENT_ID = 20000;
    public static final int PREMIUM_ORDER_UPPER_BOUND = 1000;
}
