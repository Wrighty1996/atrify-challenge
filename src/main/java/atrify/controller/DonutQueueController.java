package atrify.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import atrify.model.Delivery;
import atrify.model.Order;
import atrify.model.OrderPositionDetails;
import atrify.service.OrderService;

@RestController
public class DonutQueueController {
    @Autowired
    private OrderService orderService;

    @PostMapping("order")
    @ResponseStatus(code = HttpStatus.OK)
    void createOrder(@RequestBody @Valid Order order) {
        orderService.addOrderToQueue(order);
    }

    @PatchMapping("orders/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    void cancelOrder(@PathVariable String id) {
        orderService.cancelOrder(Integer.parseInt(id));
    }

    @GetMapping("orders/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    OrderPositionDetails getOrderPositionDetails(@PathVariable String id) {
        OrderPositionDetails orderPosition = orderService.getOrderPositionDetails(Integer.parseInt(id));
        return orderPosition;
    }

    @GetMapping("orders")
    @ResponseStatus(code = HttpStatus.OK)
    List<Order> getAllOrders() {
        final List<Order> allOrders = orderService.getAllOrders();
        return allOrders;
    }

    @PostMapping("delivery")
    @ResponseStatus(code = HttpStatus.OK)
    Delivery createDelivery() {
        final Delivery delivery = orderService.createDelivery();
        return delivery;
    }
}
