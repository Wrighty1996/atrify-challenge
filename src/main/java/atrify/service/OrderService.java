package atrify.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import atrify.domain.OrderQueue;
import atrify.model.Delivery;
import atrify.model.Order;
import atrify.model.OrderPositionDetails;
import atrify.utils.Constants;

@Service
public class OrderService {
    @Autowired
    private OrderQueue orderQueue;

    public void addOrderToQueue(Order orderToAdd) {
        setOrderPriority(orderToAdd);
        orderQueue.addOrderToQueue(orderToAdd);
    }

    public void cancelOrder(int idOfOrderToCancel) {
        final Order orderToCancel = orderQueue.getOrder(idOfOrderToCancel);
        orderToCancel.setCancelled(true);
        final int cancelledOrderQueuePosition = orderToCancel.getOrderPositionDetails().getQueuePosition();
        orderQueue.refreshOrderPositionDetails(cancelledOrderQueuePosition);
    }

    public OrderPositionDetails getOrderPositionDetails(int idOfOrder) {
        return orderQueue.getOrderPositionDetails(idOfOrder);
    }

    public List<Order> getAllOrders() {
        return orderQueue.getAllOrders();
    }

    public Delivery createDelivery() {
        return orderQueue.createDelivery();
    }

    private void setOrderPriority(Order order) {
        if (order.getClientId() < Constants.PREMIUM_ORDER_UPPER_BOUND) {
            order.setPremium(true);

        }
    }
}
