package atrify.model;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import atrify.utils.Constants;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
// Integration tests require a no arg constructor. See
// https://stackoverflow.com/questions/53191468/no-creators-like-default-construct-exist-cannot-deserialize-from-object-valu
@NoArgsConstructor
public class Order {
    @Valid
    @Min(value = Constants.MINIMUM_VALUE_OF_CLIENT_ID, message = "Order Client Id has a minimum of 1")
    @Max(value = Constants.MAXIMUM_VALUE_OF_CLIENT_ID, message = "Order Client Id has a maximum of 20000")
    private int clientId;
    private int donutQuantity;
    private boolean cancelled;
    private boolean isPremium;

    private OrderPositionDetails orderPositionDetails;

    // This constructor is only used in tests. Normally order is constructed
    // directly from customer request
    public Order(int clientId, int donutQuantity) {
        this.clientId = clientId;
        this.donutQuantity = donutQuantity;

        if (this.clientId < Constants.PREMIUM_ORDER_UPPER_BOUND) {
            this.isPremium = true;
        }
    }
}
