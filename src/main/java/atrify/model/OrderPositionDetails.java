package atrify.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
// Integration tests require a no arg constructor. See
// https://stackoverflow.com/questions/53191468/no-creators-like-default-construct-exist-cannot-deserialize-from-object-valu
@NoArgsConstructor
@AllArgsConstructor
public class OrderPositionDetails {
    private int queuePosition;
    private int approximateWaitTimeInMinutes;
}
