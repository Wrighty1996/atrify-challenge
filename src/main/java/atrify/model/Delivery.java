package atrify.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
// Integration tests require a no arg constructor. See
// https://stackoverflow.com/questions/53191468/no-creators-like-default-construct-exist-cannot-deserialize-from-object-valu
@NoArgsConstructor
public class Delivery {
    private List<Order> ordersToDeliver;
}
