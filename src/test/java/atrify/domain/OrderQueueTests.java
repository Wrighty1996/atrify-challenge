package atrify.domain;

import org.junit.jupiter.api.Test;

import atrify.model.Delivery;
import atrify.model.Order;
import atrify.model.OrderPositionDetails;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

public class OrderQueueTests {

    @Test
    void getOrder_withOnlyCorrectOrderPresentInQueue_returnsCorrectOrder() {
        // Given
        final OrderQueue orderQueue = new OrderQueue();
        final int clientId = 2;
        final Order orderToAdd = new Order(clientId, 0);
        orderQueue.addOrderToQueue(orderToAdd);

        // When
        final Order returnedOrder = orderQueue.getOrder(clientId);

        // Then
        assertThat(returnedOrder.getClientId()).isEqualTo(clientId);
    }

    @Test
    void getOrder_withVariousOrdersAndCorrectOrderPresentInQueue_returnsCorrectOrder() {
        // Given
        final OrderQueue orderQueue = new OrderQueue();
        final int clientId = 2;
        final Order notRequestedOrder1 = new Order(1, 0);
        final Order requestedOrder = new Order(clientId, 0);
        final Order notRequestedOrder2 = new Order(3, 0);
        final Order notRequestedOrder3 = new Order(2000, 0);

        final List<Order> ordersToAddToQueue = List.of(notRequestedOrder1, requestedOrder, notRequestedOrder2,
                notRequestedOrder3);

        ordersToAddToQueue.forEach((order) -> orderQueue.addOrderToQueue(order));

        // When
        final Order returnedOrder = orderQueue.getOrder(clientId);

        // Then
        assertThat(returnedOrder.getClientId()).isEqualTo(clientId);
    }

    @Test
    void getOrder_withOrderNotInQueue_returnsNull() {
        // Given
        final OrderQueue orderQueue = new OrderQueue();
        final int clientId = 2;

        // When
        final Order returnedOrder = orderQueue.getOrder(clientId);

        // Then
        assertThat(returnedOrder).isEqualTo(null);
    }

    @Test
    void getHeadFromQueue_withMultiplePremiumOrdersInQueue_returnsOrdersInCorrectOrder() {
        // Given
        final OrderQueue orderQueue = new OrderQueue();
        final int clientId1 = 1;
        final int clientId2 = 2;
        final Order orderToAdd1 = new Order(clientId1, 0);
        final Order orderToAdd2 = new Order(clientId2, 0);

        final List<Order> ordersToAddToQueue = List.of(orderToAdd1, orderToAdd2);

        ordersToAddToQueue.forEach((order) -> orderQueue.addOrderToQueue(order));

        // When
        final Order firstReturnedOrder = orderQueue.getHeadFromQueue();
        final Order secondReturnedOrder = orderQueue.getHeadFromQueue();

        // Then
        assertThat(firstReturnedOrder.getClientId()).isEqualTo(clientId1);
        assertThat(secondReturnedOrder.getClientId()).isEqualTo(clientId2);
    }

    @Test
    void getHeadFromQueue_withOnePremiumAndOneNonPremiumOrdersInQueue_returnsPremiumOrderFirst() {
        // Given
        final OrderQueue orderQueue = new OrderQueue();
        final int nonPremiumOrderId1 = 2000;
        final int premiumOrderId1 = 1;

        orderQueue.addOrderToQueue(new Order(nonPremiumOrderId1, 0));
        orderQueue.addOrderToQueue(new Order(premiumOrderId1, 0));

        // When
        final Order firstReturnedOrder = orderQueue.getHeadFromQueue();
        final Order secondReturnedOrder = orderQueue.getHeadFromQueue();

        // Then
        assertThat(firstReturnedOrder.getClientId()).isEqualTo(premiumOrderId1);
        assertThat(secondReturnedOrder.getClientId()).isEqualTo(nonPremiumOrderId1);
    }

    @Test
    void getHeadFromQueue_withMultiplePremiumAndMultipleNonPremiumOrdersInQueue_returnsOrdersInCorrectOrder() {
        // Given
        final OrderQueue orderQueue = new OrderQueue();
        final int nonPremiumOrderId1 = 2000;
        final int nonPremiumOrderId2 = 3000;
        final int premiumOrderId1 = 1;
        final int premiumOrderId2 = 2;

        final List<Integer> idsOfOrdersToAddToQueue = List.of(nonPremiumOrderId1, nonPremiumOrderId2, premiumOrderId1,
                premiumOrderId2);

        idsOfOrdersToAddToQueue.forEach((id) -> orderQueue.addOrderToQueue(new Order(id, 0)));

        // When
        final Order firstReturnedOrder = orderQueue.getHeadFromQueue();
        final Order secondReturnedOrder = orderQueue.getHeadFromQueue();
        final Order thirdReturnedOrder = orderQueue.getHeadFromQueue();
        final Order fourthReturnedOrder = orderQueue.getHeadFromQueue();

        // Then
        assertThat(firstReturnedOrder.getClientId()).isEqualTo(premiumOrderId1);
        assertThat(secondReturnedOrder.getClientId()).isEqualTo(premiumOrderId2);
        assertThat(thirdReturnedOrder.getClientId()).isEqualTo(nonPremiumOrderId1);
        assertThat(fourthReturnedOrder.getClientId()).isEqualTo(nonPremiumOrderId2);
    }

    @Test
    void getOrderPositionDetails_withMultipleOrdersInQueue_returnsCorrectQueuePosition() {
        // Given
        final OrderQueue orderQueue = new OrderQueue();
        final int order1Id = 1;
        final int order2Id = 2;
        final int order3Id = 3;
        final int order4Id = 4;

        orderQueue.addOrderToQueue(new Order(order1Id, 1));
        orderQueue.addOrderToQueue(new Order(order2Id, 2));
        orderQueue.addOrderToQueue(new Order(order3Id, 3));
        orderQueue.addOrderToQueue(new Order(order4Id, 0));

        // When
        final OrderPositionDetails position = orderQueue.getOrderPositionDetails(order3Id);

        // Then
        assertThat(position.getQueuePosition()).isEqualTo(3);
    }

    @Test
    void getOrderPositionDetails_withMultipleOrdersInQueue_returnsCorrectEstimatedWaitTimeInMinutes() {
        // Given
        final OrderQueue orderQueue = new OrderQueue();
        final int order1Id = 1;
        final int order2Id = 2;
        final int order3Id = 3;
        final int order4Id = 4;

        final List<Integer> idsOfOrdersToAddToQueue = List.of(order1Id, order2Id, order3Id,
                order4Id);

        idsOfOrdersToAddToQueue.forEach((id) -> orderQueue.addOrderToQueue(new Order(id, 50)));

        // When
        final OrderPositionDetails position = orderQueue.getOrderPositionDetails(order4Id);

        // Then
        assertThat(position.getApproximateWaitTimeInMinutes()).isEqualTo(20);
    }

    @Test
    void getOrderPositionDetails_withOneOrderInQueue_returnsCorrectEstimatedWaitTimeInMinutes() {
        // Given
        final OrderQueue orderQueue = new OrderQueue();
        final int order1Id = 1;

        orderQueue.addOrderToQueue(new Order(order1Id, 10));

        // When
        final OrderPositionDetails position = orderQueue.getOrderPositionDetails(order1Id);

        // Then
        assertThat(position.getApproximateWaitTimeInMinutes()).isEqualTo(5);
    }

    @Test
    void getOrderPositionDetails_withMultipleSmallOrdersInQueue_returnsCorrectEstimatedWaitTimeInMinutes() {
        // Given
        final OrderQueue orderQueue = new OrderQueue();
        final int order1Id = 1;
        final int order2Id = 2;
        final int order3Id = 3;
        final int expectedMinutesToWait = 5;

        orderQueue.addOrderToQueue(new Order(order1Id, 1));
        orderQueue.addOrderToQueue(new Order(order2Id, 2));
        orderQueue.addOrderToQueue(new Order(order3Id, 3));

        // When
        final OrderPositionDetails position = orderQueue.getOrderPositionDetails(order3Id);

        // Then
        assertThat(position.getApproximateWaitTimeInMinutes()).isEqualTo(expectedMinutesToWait);
    }

    @Test
    void getAllOrderPositionDetails_withOneOrderInQueue_returnsCorrectOrderPositionDetail() {
        // Given
        final OrderQueue orderQueue = new OrderQueue();
        final int order1Id = 1;
        final int expectedNumberOfOrderPositionDetails = 1;

        orderQueue.addOrderToQueue(new Order(order1Id, 1));

        // When
        final List<OrderPositionDetails> allOrderPositions = orderQueue.getAllOrderPositionDetails();

        // Then
        assertThat(allOrderPositions.size()).isEqualTo(expectedNumberOfOrderPositionDetails);
    }

    @Test
    void getAllOrderPositionDetails_withMultipleOrdersInQueue_returnsCorrectOrderPositionDetail() {
        // Given
        final OrderQueue orderQueue = new OrderQueue();
        final int order1Id = 1;
        final int order2Id = 2;
        final int order3Id = 3;

        final List<Integer> idsOfOrdersToAddToQueue = List.of(order1Id, order2Id, order3Id);

        idsOfOrdersToAddToQueue.forEach((id) -> orderQueue.addOrderToQueue(new Order(id, 1)));

        final int expectedNumberOfOrderPositionDetails = 3;

        // When
        final List<OrderPositionDetails> allOrderPositions = orderQueue.getAllOrderPositionDetails();

        // Then
        assertThat(allOrderPositions.size()).isEqualTo(expectedNumberOfOrderPositionDetails);
    }

    @Test
    void createDelivery_withMultipleOrdersWhichFitIntoOneDelivery_returnsCorrectDeliveryObject() {
        // Given
        final OrderQueue orderQueue = new OrderQueue();
        final int order1Id = 1;
        final int order2Id = 2;
        final int order3Id = 3;

        final List<Integer> idsOfOrdersToAddToQueue = List.of(order1Id, order2Id, order3Id);

        final int expectedNumberOfItemsInDelivery = 3;
        final List<Integer> expectedOrderIds = List.of(order1Id, order2Id, order3Id);
        final int expectedNumberOfRemainingItemsInQueue = 0;

        idsOfOrdersToAddToQueue.forEach((id) -> orderQueue.addOrderToQueue(new Order(id, 1)));

        // When
        final Delivery returnedDelivery = orderQueue.createDelivery();
        final List<Integer> returnedIdsOfOrdersInDelivery = returnedDelivery.getOrdersToDeliver().stream()
                .map((order) -> order.getClientId()).toList();

        // Then
        assertThat(returnedDelivery.getOrdersToDeliver().size()).isEqualTo(expectedNumberOfItemsInDelivery);
        assertThat(returnedIdsOfOrdersInDelivery).isEqualTo(expectedOrderIds);
        assertThat(orderQueue.getAllOrders().size()).isEqualTo(expectedNumberOfRemainingItemsInQueue);
    }

    @Test
    void createDelivery_withMultipleOrdersNotAllOfWhichFitIntoOneDelivery_returnsCorrectDeliveryObjectAndLeavesCorrectItemsInQueue() {
        // Given
        final OrderQueue orderQueue = new OrderQueue();
        final int order1Id = 1;
        final int order2Id = 2;
        final int order3Id = 3;

        final int expectedNumberOfItemsInDelivery = 2;
        final List<Integer> expectedOrderIds = List.of(order1Id, order2Id);
        final int expectedNumberOfRemainingItemsInQueue = 1;

        orderQueue.addOrderToQueue(new Order(order1Id, 25));
        orderQueue.addOrderToQueue(new Order(order2Id, 25));
        orderQueue.addOrderToQueue(new Order(order3Id, 5));

        // When
        final Delivery returnedDelivery = orderQueue.createDelivery();
        final List<Integer> returnedIdsOfOrdersInDelivery = returnedDelivery.getOrdersToDeliver().stream()
                .map((order) -> order.getClientId()).toList();

        // Then
        assertThat(returnedDelivery.getOrdersToDeliver().size()).isEqualTo(expectedNumberOfItemsInDelivery);
        assertThat(returnedIdsOfOrdersInDelivery).isEqualTo(expectedOrderIds);
        assertThat(orderQueue.getAllOrders().size()).isEqualTo(expectedNumberOfRemainingItemsInQueue);

        final Order remainingOrderInQueue = orderQueue.getAllOrders().get(0);
        assertThat(remainingOrderInQueue.getClientId()).isEqualTo(order3Id);
    }

    @Test
    void createDelivery_withMultipleOrdersNoneOfWhichFitIntoOneDelivery_returnsCorrectDeliveryObjectAndLeavesCorrectItemsInQueue() {
        // Given
        final OrderQueue orderQueue = new OrderQueue();
        final int order1Id = 1;
        final int order2Id = 2;
        final int order3Id = 3;

        final int expectedNumberOfItemsInDelivery = 0;
        final int expectedNumberOfRemainingItemsInQueue = 3;

        final List<Integer> expectedIdsOfItemsRemainingInQueue = List.of(order1Id, order2Id, order3Id);

        orderQueue.addOrderToQueue(new Order(order1Id, 51));
        orderQueue.addOrderToQueue(new Order(order2Id, 52));
        orderQueue.addOrderToQueue(new Order(order3Id, 53));

        // When
        final Delivery returnedDelivery = orderQueue.createDelivery();
        final List<Integer> returnedIdsOfOrdersInDelivery = returnedDelivery.getOrdersToDeliver().stream()
                .map((order) -> order.getClientId()).toList();
        final List<Integer> idsOfItemsStilInQueue = orderQueue.getAllOrders().stream()
                .map((order) -> order.getClientId()).toList();

        // Then
        assertThat(returnedDelivery.getOrdersToDeliver().size()).isEqualTo(expectedNumberOfItemsInDelivery);
        assertThat(returnedIdsOfOrdersInDelivery).isEqualTo(new ArrayList<Integer>());

        assertThat(orderQueue.getAllOrders().size()).isEqualTo(expectedNumberOfRemainingItemsInQueue);
        assertThat(idsOfItemsStilInQueue).isEqualTo(expectedIdsOfItemsRemainingInQueue);
    }
}
