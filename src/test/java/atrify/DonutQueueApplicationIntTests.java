package atrify;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;

import atrify.model.Delivery;
import atrify.model.Order;
import atrify.model.OrderPositionDetails;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class DonutQueueApplicationIntTests {
        @LocalServerPort
        int randomServerPort;
        String baseUrlForGetRequest;
        String baseUrlForPostRequest;
        String baseUrlForDeliveryPostRequest;

        RestTemplate restTemplate;
        HttpComponentsClientHttpRequestFactory requestFactory;

        @BeforeEach
        public void initTestData() {
                restTemplate = new RestTemplate();
                requestFactory = new HttpComponentsClientHttpRequestFactory();
                restTemplate.setRequestFactory(requestFactory);

                baseUrlForGetRequest = "http://localhost:" + randomServerPort + "/orders/";
                baseUrlForPostRequest = "http://localhost:" + randomServerPort + "/order/";
                baseUrlForDeliveryPostRequest = "http://localhost:" + randomServerPort + "/delivery/";
        }

        @org.junit.jupiter.api.Test
        public void createOrderEndpoint_withOneOrderCreated_correctlyAddsOrderToQueue()
                        throws URISyntaxException {
                // Given
                final int clientIdOfOrder1 = 123;

                final URI uriForPostRequest = new URI(baseUrlForPostRequest);

                final HttpEntity<Object> postRequest1 = new HttpEntity<Object>(new Order(clientIdOfOrder1, 1));

                // When
                // MAKE POST REQUEST
                final ResponseEntity<String> postRequestResult1 = restTemplate.postForEntity(uriForPostRequest,
                                postRequest1,
                                String.class);

                // MAKE GET REQUEST
                final ResponseEntity<List<Order>> getRequestResult = restTemplate.exchange(
                                baseUrlForGetRequest, HttpMethod.GET, null,
                                new ParameterizedTypeReference<List<Order>>() {
                                });

                final List<Order> resultBody = getRequestResult.getBody();

                // Then
                assertThat(postRequestResult1.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
                assertThat(getRequestResult.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());

                assertThat(resultBody).isNotNull();
                assertThat(resultBody.size()).isEqualTo(1);
                final Order returnedOrder = resultBody.get(0);
                assertThat(returnedOrder.getClientId()).isEqualTo(clientIdOfOrder1);
        }

        @org.junit.jupiter.api.Test
        public void getOrderPositionEndpoint_withTwoOrdersCreated_appliesReturnsCorrectPositionAndEstimatedTimeForOrder()
                        throws URISyntaxException {
                // Given
                final int clientIdOfOrder1 = 123;
                final int clientIdOfOrder2 = 456;

                final URI uriForPostRequest = new URI(baseUrlForPostRequest);

                final HttpEntity<Object> postRequest1 = new HttpEntity<Object>(new Order(clientIdOfOrder1, 1));
                final HttpEntity<Object> postRequest2 = new HttpEntity<Object>(new Order(clientIdOfOrder2, 1));

                final String requestForOrder2 = baseUrlForGetRequest + "456";

                // When
                // MAKE POST REQUESTS
                final ResponseEntity<String> postRequestResult1 = restTemplate.postForEntity(uriForPostRequest,
                                postRequest1,
                                String.class);

                final ResponseEntity<String> postRequestResult2 = restTemplate.postForEntity(uriForPostRequest,
                                postRequest2,
                                String.class);

                // MAKE GET REQUEST
                final ResponseEntity<OrderPositionDetails> getRequestForOrder2Result = restTemplate.exchange(
                                requestForOrder2, HttpMethod.GET, null,
                                new ParameterizedTypeReference<OrderPositionDetails>() {
                                });

                final OrderPositionDetails orderPositionDetailsFromGetRequest = getRequestForOrder2Result.getBody();

                // Then
                assertThat(postRequestResult1.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
                assertThat(postRequestResult2.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
                assertThat(getRequestForOrder2Result.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());

                assertThat(getRequestForOrder2Result).isNotNull();
                assertThat(orderPositionDetailsFromGetRequest.getQueuePosition()).isEqualTo(2);
                assertThat(orderPositionDetailsFromGetRequest.getApproximateWaitTimeInMinutes()).isEqualTo(5);
        }

        @org.junit.jupiter.api.Test
        public void cancelOrderEndpoint_withOneOrderCancelled_correctlyUpdatesThePositionsOfOtherOrders()
                        throws URISyntaxException {
                // Given
                final int clientIdOfOrder1 = 456;
                final int clientIdOfOrder2 = 789;

                final String urlToCancelOrder1 = "http://localhost:" + randomServerPort + "/orders/"
                                + clientIdOfOrder1;
                final URI uriForPostRequest = new URI(baseUrlForPostRequest);

                final HttpEntity<Object> postRequest1 = new HttpEntity<Object>(new Order(clientIdOfOrder1, 1));
                final HttpEntity<Object> postRequest2 = new HttpEntity<Object>(new Order(clientIdOfOrder2, 1));
                final HttpEntity<Object> patchRequest = new HttpEntity<Object>(null);

                // When
                // MAKE POST REQUEST
                final ResponseEntity<String> postRequestResult1 = restTemplate.postForEntity(uriForPostRequest,
                                postRequest1,
                                String.class);

                final ResponseEntity<String> postRequestResult2 = restTemplate.postForEntity(uriForPostRequest,
                                postRequest2,
                                String.class);

                // MAKE PATCH REQUEST
                restTemplate.patchForObject(urlToCancelOrder1,
                                patchRequest,
                                ResponseEntity.class);

                // MAKE GET REQUEST
                final ResponseEntity<List<Order>> getRequestResult = restTemplate.exchange(
                                baseUrlForGetRequest, HttpMethod.GET, null,
                                new ParameterizedTypeReference<List<Order>>() {
                                });

                final List<Order> resultBody = getRequestResult.getBody();

                // Then
                assertThat(postRequestResult1.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
                assertThat(postRequestResult2.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
                assertThat(getRequestResult.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
                assertThat(resultBody.size()).isEqualTo(1);
                final Order returnedOrder = resultBody.get(0);
                assertThat(returnedOrder.getClientId()).isEqualTo(clientIdOfOrder2);
        }

        @org.junit.jupiter.api.Test
        public void createDeliveryEndpoint_withMultipleOrdersInQueue_returnsDeliveryWithCorrectOrders()
                        throws URISyntaxException {
                // Given
                final int clientIdOfOrder1 = 123;
                final int clientIdOfOrder2 = 456;
                final int clientIdOfOrder3 = 789;

                final URI uriForPostRequest = new URI(baseUrlForPostRequest);

                final HttpEntity<Object> postRequest1 = new HttpEntity<Object>(new Order(clientIdOfOrder1, 25));
                final HttpEntity<Object> postRequest2 = new HttpEntity<Object>(new Order(clientIdOfOrder2, 25));
                final HttpEntity<Object> postRequest3 = new HttpEntity<Object>(new Order(clientIdOfOrder3, 10));

                final List<Integer> expectedIdsToReturn = List.of(clientIdOfOrder1, clientIdOfOrder2);
                // When
                // MAKE POST REQUEST
                final ResponseEntity<String> postRequestResult1 = restTemplate.postForEntity(uriForPostRequest,
                                postRequest1,
                                String.class);

                final ResponseEntity<String> postRequestResult2 = restTemplate.postForEntity(uriForPostRequest,
                                postRequest2,
                                String.class);

                final ResponseEntity<String> postRequestResult3 = restTemplate.postForEntity(uriForPostRequest,
                                postRequest3,
                                String.class);

                // MAKE DELIVERY REQUEST
                final ResponseEntity<Delivery> getDeliveryRequestResult = restTemplate.exchange(
                                baseUrlForDeliveryPostRequest, HttpMethod.POST, null,
                                new ParameterizedTypeReference<Delivery>() {
                                });

                final Delivery resultBody = getDeliveryRequestResult.getBody();

                // Then
                assertThat(postRequestResult1.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
                assertThat(postRequestResult2.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
                assertThat(postRequestResult3.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());

                assertThat(getDeliveryRequestResult.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());

                assertThat(resultBody.getOrdersToDeliver().size()).isEqualTo(2);
                final List<Order> ordersInDelivery = resultBody.getOrdersToDeliver();
                final List<Integer> idsOfOrders = ordersInDelivery.stream().map((order) -> order.getClientId())
                                .toList();
                assertThat(idsOfOrders).isEqualTo(expectedIdsToReturn);

        }
}